//
//  SceneDelegate.swift
//  Messenger
//
//  Created by Muhammad on 29.11.2022.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let window = UIWindow(windowScene: windowScene)
        
        let tabBarController = UITabBarController()
        
        let chatsViewController = ChatsViewController()
        
        let chatsNavigationController = UINavigationController(rootViewController: chatsViewController)
        chatsNavigationController.navigationBar.prefersLargeTitles = true
        chatsNavigationController.tabBarItem = UITabBarItem(
            title: "Чаты",
            image: UIImage(systemName: "message"),
            selectedImage: UIImage(systemName: "message.fill")
        )
        
        let profileViewController = ProfileViewController()
        profileViewController.tabBarItem = UITabBarItem(
            title: "Профиль",
            image: UIImage(systemName: "person"),
            selectedImage: UIImage(systemName: "person.fill")
        )
        
        tabBarController.viewControllers = [chatsNavigationController, profileViewController]
        
        window.rootViewController = tabBarController
        self.window = window
        window.makeKeyAndVisible()
    }

}

