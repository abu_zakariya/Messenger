//
//  ChatsViewController.swift
//  Messenger
//
//  Created by Muhammad on 28.12.2022.
//

import UIKit
import FirebaseAuth

final class ChatsViewController: UIViewController {
    
    private let mainView = ChatsView()
    
    private let items = [
        ChatsModel(icon: "google", title: "Djoni", subtitle: "ddffdbsfdbvb", timestamp: "12:15"),
        ChatsModel(icon: "facebook", title: "abdul", subtitle: "Send me some jams, I’ve been listening to way too much bad bunny", timestamp: "16:15"),
        ChatsModel(icon: "apple", title: "таксист", subtitle: "гассвёфв", timestamp: "23:44")
    ]
    
    override func loadView() {
        view = mainView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Messages"
        navigationItem.searchController = UISearchController()
        navigationItem.largeTitleDisplayMode = .always
        navigationItem.hidesSearchBarWhenScrolling = false
        
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(newChatButtonTapped))
        
        mainView.tableView.register(ChatsTableViewCell.self, forCellReuseIdentifier: ChatsTableViewCell.reuseId)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if FirebaseAuth.Auth.auth().currentUser == nil {
            let vc = LoginViewController()
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        }
    }
    
    @objc
    private func newChatButtonTapped() {
        let vc = NewChatViewController()
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .formSheet
        present(navigationController, animated: true, completion: nil)
    }
}

extension ChatsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ChatsTableViewCell.reuseId, for: indexPath) as? ChatsTableViewCell else {
            fatalError("Can not dequeue ChatsTableViewCell")
        }
        
        let item = items[indexPath.row]
        cell.configure(item: item)
        cell.selectionStyle = .none
        
        return cell
    }
}

extension ChatsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ChatViewController()
        vc.title = items[indexPath.row].title
        vc.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(vc, animated: true)
    }
}
