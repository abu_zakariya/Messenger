//
//  ChatsModel.swift
//  Messenger
//
//  Created by Muhammad on 28.12.2022.
//

struct ChatsModel {
    let icon: String
    let title: String
    let subtitle: String
    let timestamp: String
}
