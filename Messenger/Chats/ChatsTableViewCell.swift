//
//  ChatsTableViewCell.swift
//  Messenger
//
//  Created by Muhammad on 28.12.2022.
//

import UIKit

final class ChatsTableViewCell: UITableViewCell {
    
    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = Metrics.halfModule * 5
        return imageView
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15)
        label.textColor = .systemGray
        label.numberOfLines = 2
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let timestampLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15)
        label.textColor = .systemGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let disclosureIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "disclosure")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    static var reuseId = "ChatsTableViewCell"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(item: ChatsModel) {
        iconImageView.image = UIImage(named: item.icon)
        titleLabel.text = item.title
        subtitleLabel.text = item.subtitle
        timestampLabel.text = item.timestamp
    }
    
}

// MARK: - Layout
extension ChatsTableViewCell {
    
    private func setupLayout() {
        setupIconImageViewLayout()
        setupTitleLabelLayout()
        setupSubtitleLayout()
        setupDisclosureIconLayout()
        setupTimestampLabelLayout()
    }
    
    private func setupIconImageViewLayout() {
        contentView.addSubview(iconImageView)
        
        iconImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        iconImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Metrics.module * 3).isActive = true
        iconImageView.heightAnchor.constraint(equalToConstant: Metrics.module * 5).isActive = true
        iconImageView.widthAnchor.constraint(equalToConstant: Metrics.module * 5).isActive = true
    }
    
    private func setupTitleLabelLayout() {
        contentView.addSubview(titleLabel)
        
        titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Metrics.module * 1.5).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: Metrics.halfModule * 3).isActive = true
    }
    
    private func setupSubtitleLayout() {
        contentView.addSubview(subtitleLabel)
        
        subtitleLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor).isActive = true
        subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Metrics.halfModule / 2).isActive = true
        subtitleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Metrics.halfModule * 5).isActive = true
    }
    
    private func setupDisclosureIconLayout() {
        contentView.addSubview(disclosureIcon)
        
        disclosureIcon.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Metrics.halfModule * 5).isActive = true
        disclosureIcon.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Metrics.module).isActive = true
    }
    
    private func setupTimestampLabelLayout() {
        contentView.addSubview(timestampLabel)
        
        timestampLabel.trailingAnchor.constraint(equalTo: disclosureIcon.leadingAnchor, constant: -Metrics.halfModule).isActive = true
        timestampLabel.topAnchor.constraint(equalTo: disclosureIcon.topAnchor).isActive = true
    }
}
