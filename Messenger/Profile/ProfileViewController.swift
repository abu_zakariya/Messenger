//
//  ProfileViewController.swift
//  Messenger
//
//  Created by Muhammad on 02.01.2023.
//

import UIKit
import FirebaseAuth

final class ProfileViewController: UIViewController {

    private let mainView = ProfileView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        mainView.emailLabel.text = FirebaseAuth.Auth.auth().currentUser?.email
        
        mainView.logoutButton.addTarget(self, action: #selector(logoutButtonTapped), for: .touchDown)
    }
    
    override func loadView() {
        view = mainView
    }
    
    @objc
    private func logoutButtonTapped() {
        let alert = UIAlertController(title: "Выйти из аккаунта?", message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Да", style: .destructive, handler: nil)
        let cancelAction = UIAlertAction(title: "Нет", style: .default, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }

}
