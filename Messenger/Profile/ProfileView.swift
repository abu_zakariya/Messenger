//
//  ProfileView.swift
//  Messenger
//
//  Created by Muhammad on 02.01.2023.
//

import UIKit

final class ProfileView: UIView {
    
    private let avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.borderWidth = 2
        imageView.image = UIImage(named: "iconperson")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isUserInteractionEnabled = true
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = Metrics.doubleModule * 5
        return imageView
    }()
    
    let emailLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 25)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let logoutButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .red
        button.layer.cornerRadius = Metrics.module * 2
        button.setTitleColor(.white, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Log out", for: .normal)
        let inset = Metrics.halfModule * 3
        button.contentEdgeInsets = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Layout
    private func setupLayout() {
        setupAvatarImageViewLayout()
        setupEmailLabelLayout()
        setuplogButtonLayout()
    }
    
    private func setupAvatarImageViewLayout() {
        addSubview(avatarImageView)
        
        avatarImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        avatarImageView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: Metrics.halfModule * 2).isActive = true
        avatarImageView.heightAnchor.constraint(equalToConstant: Metrics.doubleModule * 10).isActive = true
        avatarImageView.widthAnchor.constraint(equalToConstant: Metrics.doubleModule * 10).isActive = true
    }
    
    private func setupEmailLabelLayout() {
        addSubview(emailLabel)
        
        emailLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        emailLabel.topAnchor.constraint(equalTo: avatarImageView.bottomAnchor, constant: Metrics.halfModule * 6).isActive = true
    }
    
    private func setuplogButtonLayout() {
        addSubview(logoutButton)
        
        logoutButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        logoutButton.topAnchor.constraint(equalTo: emailLabel.bottomAnchor, constant: Metrics.halfModule * 6).isActive = true
    }
    
}
