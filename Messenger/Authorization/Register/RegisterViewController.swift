//
//  RegisterViewController.swift
//  Messenger
//
//  Created by Muhammad on 08.12.2022.
//

import UIKit
import FirebaseAuth
import JGProgressHUD

final class RegisterViewController: UIViewController {
    
    private let mainView = RegisterView()
    
    private let spinner = JGProgressHUD(style: .dark)
    
    override func loadView() {
        super.loadView()
        
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.registerButton.addTarget(self, action: #selector(registerButtonTapped), for: .touchDown)
        
        view.backgroundColor = .white
        
        setupAvatarImageView()
        
        mainView.userNameTextField.delegate = self
        mainView.confirmPasswordTextField.delegate = self
        mainView.confirmPasswordTextField.isSecureTextEntry = true
        mainView.passwordTextField.isSecureTextEntry = true
        mainView.passwordTextField.delegate = self
        mainView.emailTextField.delegate = self
    }
    
    private func setupAvatarImageView() {
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(avatarImageViewTapped))
        
        mainView.avatarImageView.addGestureRecognizer(tapGesture)
    }
    
    @objc private func avatarImageViewTapped() {
        let actionSheet = UIAlertController(title: "Выберите фото", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Снять фото", style: .default) { [weak self] _ in
            self?.showCamera()
        }
        let galleryAction = UIAlertAction(title: "Выбрать фото", style: .default) { [weak self] _ in
            self?.showGalery()
        }
        let cancelAction = UIAlertAction(title: "Отменить", style: .destructive)
        
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(galleryAction)
        actionSheet.addAction(cancelAction)
        
        present(actionSheet, animated: false, completion: nil)
    }
    
    private func showGalery() {
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true)
    }
    
    private func showCamera() {
        let picker = UIImagePickerController()
        picker.sourceType = .camera
        picker.delegate = self
        present(picker, animated: true)
    }
    
    @objc
    private func registerButtonTapped() {
        guard let email = mainView.emailTextField.text,
              let password = mainView.passwordTextField.text,
              let userName = mainView.userNameTextField.text,
              let confirm = mainView.confirmPasswordTextField.text,
              !email.isEmpty,
              !password.isEmpty,
              !userName.isEmpty,
              !confirm.isEmpty
        else {
            return showAlert(title: "Строки не могут быть пустыми", message: "")
        }
        
        if email.count < 6 || password.count < 6 || userName.count < 6 || confirm.count < 6 {
            return showAlert(title: "Не меньше 6 символов", message: "")
        }
        
        if password != confirm {
            return showAlert(title: "Пароли не совпадают", message: "")
        }
        
        spinner.show(in: mainView)
        
        let _ = RegisterData(email: email, password: password, username: userName)
        FirebaseAuth.Auth.auth().createUser(withEmail: email, password: password) { [weak self] result, error in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                self.spinner.dismiss(animated: false)
            }
            
            guard result != nil, error == nil else {
                self.showAlert(title: "Ошибка регистрации", message: "")
                return
            }
            self.dismiss(animated: true)
        }
    }
    
    private func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ок", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}

extension RegisterViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == mainView.userNameTextField {
            mainView.emailTextField.becomeFirstResponder()
        }
        if textField == mainView.emailTextField {
            mainView.passwordTextField.becomeFirstResponder()
        }
        if textField == mainView.passwordTextField {
            mainView.confirmPasswordTextField.becomeFirstResponder()
        }
        if textField == mainView.confirmPasswordTextField {
            registerButtonTapped()
        }
        return true
    }
}

extension RegisterViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]
    ) {
        guard let image = info[.originalImage] as? UIImage else { return }
        mainView.avatarImageView.image = image
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
