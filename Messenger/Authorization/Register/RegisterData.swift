//
//  RegisterData.swift
//  Messenger
//
//  Created by Muhammad on 08.12.2022.
//

struct RegisterData {
    let email: String
    let password: String
    let username: String
}
