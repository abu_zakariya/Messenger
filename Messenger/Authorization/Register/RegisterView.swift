//
//  RegisterView.swift
//  Messenger
//
//  Created by Muhammad on 08.12.2022.
//

import UIKit

final class RegisterView: UIView {

    lazy var userNameTextField = AuthorizationViewFactory.makeTextField(placeholder: "Username")
    lazy var emailTextField = AuthorizationViewFactory.makeTextField(placeholder: "Email")
    lazy var passwordTextField = AuthorizationViewFactory.makeTextField(placeholder: "Password")
    lazy var confirmPasswordTextField = AuthorizationViewFactory.makeTextField(placeholder: "Confirm password")
    lazy var registerButton = AuthorizationViewFactory.makeActionButton(string: "Register")
    
    private let socialNetworksView = SocialNetworksView()
    
    private let scrollView: UIScrollView = {
        let scrollView =  UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private let contentView: UIView = {
        let contentView = UIView()
        contentView.translatesAutoresizingMaskIntoConstraints = false
        return contentView
    }()
    
     let avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.borderWidth = 2
        imageView.image = UIImage(named: "iconperson")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isUserInteractionEnabled = true
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = Metrics.doubleModule * 4
        return imageView
    }()
    
    private let textFieldsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = Metrics.module * 2
        return stackView
    }()
    
    private let haveAccountStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = Metrics.halfModule
        return stackView
    }()
    
    private let haveAccountLabel: UILabel = {
        let label = UILabel()
        label.text = "Already have an account?"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name:"Urbanist-SemiBold", size: 30)
        return label
    }()
    
    private let loginButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.systemGreen, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Login Now", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Layout
    private func setupLayout() {
        setupScrollViewLayout()
        setupContentViewLayout()
        setupTextFieldsStackViewLayout()
        setupRegisterButtonLayout()
        setupSocialNetworksView()
        setupHaveAccountStackViewLayout()
        setupAvatarImageView()
    }
    
    private func setupScrollViewLayout() {
        addSubview(scrollView)
        
        scrollView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    private func setupContentViewLayout() {
        scrollView.addSubview(contentView)
        
        contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
    }
    
    private func setupAvatarImageView() {
        contentView.addSubview(avatarImageView)
        
        avatarImageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        avatarImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Metrics.doubleModule * 2).isActive = true
        avatarImageView.widthAnchor.constraint(equalToConstant: Metrics.doubleModule * 8).isActive = true
        avatarImageView.heightAnchor.constraint(equalToConstant: Metrics.doubleModule * 8).isActive = true
    }

    private func setupTextFieldsStackViewLayout() {
        contentView.addSubview(textFieldsStackView)
        
        textFieldsStackView.addArrangedSubview(userNameTextField)
        textFieldsStackView.addArrangedSubview(emailTextField)
        textFieldsStackView.addArrangedSubview(passwordTextField)
        textFieldsStackView.addArrangedSubview(confirmPasswordTextField)
        
        textFieldsStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Metrics.module * 25).isActive = true
        textFieldsStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Metrics.halfModule * 5).isActive = true
        textFieldsStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Metrics.halfModule * 5).isActive =
        true
        
        userNameTextField.heightAnchor.constraint(equalToConstant: Metrics.module * 7).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: Metrics.module * 7).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: Metrics.module * 7).isActive = true
        confirmPasswordTextField.heightAnchor.constraint(equalToConstant: Metrics.module * 7).isActive = true
    }
    
    private func setupRegisterButtonLayout() {
        contentView.addSubview(registerButton)
        
        registerButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Metrics.halfModule * 5).isActive = true
        registerButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Metrics.halfModule * 5).isActive = true
        registerButton.topAnchor.constraint(equalTo: textFieldsStackView.bottomAnchor, constant: Metrics.halfModule * 15).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: Metrics.module * 7).isActive = true
    }
    
    private func setupSocialNetworksView() {
        contentView.addSubview(socialNetworksView)

        socialNetworksView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        socialNetworksView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        socialNetworksView.topAnchor.constraint(equalTo: registerButton.bottomAnchor).isActive = true
    }
    
    private func setupHaveAccountStackViewLayout() {
        contentView.addSubview(haveAccountStackView)
        
        haveAccountStackView.addArrangedSubview(haveAccountLabel)
        haveAccountStackView.addArrangedSubview(loginButton)
        
        haveAccountStackView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        haveAccountStackView.topAnchor.constraint(equalTo: socialNetworksView.bottomAnchor, constant: Metrics.module * 7).isActive = true
        haveAccountStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,constant: -Metrics.module * 4).isActive = true
    }
}
