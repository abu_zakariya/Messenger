//
//  LoginWithView.swift
//  Messenger
//
//  Created by Muhammad on 01.12.2022.
//

import UIKit

protocol SocialNetworksViewDelegate: AnyObject {
    func facebookButtonTapped()
    func googleButtonTapped()
    func appleButtonTapped()
}

final class SocialNetworksView: UIView {
    
    weak var delegate: SocialNetworksViewDelegate?
    
    private lazy var facebookButton = AuthorizationViewFactory.makeButton(image: "facebook")
    private lazy var googleButton = AuthorizationViewFactory.makeButton(image: "google")
    private lazy var appleButton = AuthorizationViewFactory.makeButton(image: "apple")
    
    private lazy var leftSeparator = AuthorizationViewFactory.makeSeparatorView()
    private lazy var rightSeparator = AuthorizationViewFactory.makeSeparatorView()
    
    private let buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = Metrics.module
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private let loginWithLabel: UILabel = {
        let loginWithLabel = UILabel()
        loginWithLabel.text = "or Login with"
        loginWithLabel.textColor = UIColor(displayP3Red: 0.416, green: 0.439, blue: 0.486, alpha: 1)
        loginWithLabel.translatesAutoresizingMaskIntoConstraints = false
        loginWithLabel.font = UIFont(name:"Urbanist-SemiBold", size: 8)
        return loginWithLabel
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLayout()
        setupActions()
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupLayout() {
        setupHeaderLayout()
        setupButtonsStackViewLayout()
    }
    
    private func setupHeaderLayout() {
        addSubview(leftSeparator)
        addSubview(rightSeparator)
        addSubview(loginWithLabel)
        
        leftSeparator.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Metrics.halfModule * 5).isActive = true
        leftSeparator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        leftSeparator.trailingAnchor.constraint(equalTo: loginWithLabel.leadingAnchor, constant: -Metrics.halfModule * 3).isActive = true
        leftSeparator.centerYAnchor.constraint(equalTo: loginWithLabel.centerYAnchor).isActive = true
        
        loginWithLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        loginWithLabel.topAnchor.constraint(equalTo: topAnchor, constant: Metrics.halfModule * 9).isActive = true
        
        rightSeparator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        rightSeparator.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Metrics.halfModule * 5).isActive = true
        rightSeparator.leadingAnchor.constraint(equalTo: loginWithLabel.trailingAnchor, constant: Metrics.halfModule * 3).isActive = true
        rightSeparator.centerYAnchor.constraint(equalTo: loginWithLabel.centerYAnchor).isActive = true
    }
    
    private func setupButtonsStackViewLayout() {
        addSubview(buttonsStackView)
        
        buttonsStackView.addArrangedSubview(facebookButton)
        buttonsStackView.addArrangedSubview(googleButton)
        buttonsStackView.addArrangedSubview(appleButton)
        
        facebookButton.heightAnchor.constraint(equalToConstant: Metrics.module * 7).isActive = true
        
        googleButton.heightAnchor.constraint(equalToConstant: Metrics.module * 7).isActive = true
        
        appleButton.heightAnchor.constraint(equalToConstant: Metrics.module * 7).isActive = true
        
        buttonsStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Metrics.halfModule * 5).isActive = true
        buttonsStackView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -Metrics.halfModule * 5).isActive = true
        buttonsStackView.topAnchor.constraint(equalTo: loginWithLabel.bottomAnchor, constant: Metrics.halfModule * 5).isActive = true
        buttonsStackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    @objc
    private func facebookButtonTapped() {
        delegate?.facebookButtonTapped()
    }
    
    @objc
    private func googleButtonTapped() {
        delegate?.googleButtonTapped()
    }
    
    @objc
    private func appleButtonTapped() {
        delegate?.appleButtonTapped()
    }
    
    private func setupActions() {
        facebookButton.addTarget(self, action: #selector(facebookButtonTapped), for: .touchDown)
        googleButton.addTarget(self, action: #selector(googleButtonTapped), for: .touchDown)
        appleButton.addTarget(self, action: #selector(appleButtonTapped), for: .touchDown)
    }
}
