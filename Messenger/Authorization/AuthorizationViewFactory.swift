//
//  AuthorizationViewFactory.swift
//  Messenger
//
//  Created by Muhammad on 01.12.2022.
//

import UIKit

struct AuthorizationViewFactory {
    
    static func makeTextField(placeholder: String) -> UITextField {
        let textField = UITextField()
        textField.layer.borderColor = UIColor.black.cgColor
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.layer.cornerRadius = Metrics.module * 2
        textField.layer.borderWidth = 0.5
        textField.layer.borderColor = .none
        textField.placeholder = placeholder
        let view = UIView(frame: .init(x: 0, y: 0, width: 18, height: 0))
        textField.leftView = view
        view.backgroundColor = .gray
        textField.leftViewMode = .always
        return textField
    }
    
    static func makeSeparatorView() -> UIView {
        let separatorView = UIView()
        separatorView.backgroundColor = .gray
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        return separatorView
    }
    
    static func makeButton(image: String) -> UIButton {
        let button = UIButton()
        button.setImage(UIImage(named: image), for: .normal)
        button.layer.borderWidth = 1
        button.layer.backgroundColor = .none
        button.layer.cornerRadius = Metrics.halfModule * 3
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
    
    static func makeActionButton(string: String) -> UIButton {
        let button = UIButton()
        button.backgroundColor = .black
        button.setTitle(string, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = Metrics.module * 2
        button.clipsToBounds = true
        return button
    }
    
    static func makeLabel(string: String) -> UILabel {
        let label = UILabel()
        label.numberOfLines = 2
        label.text = string
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name:"HelveticaNeue-Bold", size: 32)
        return label
    }
}

