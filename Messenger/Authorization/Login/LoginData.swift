//
//  LoginData.swift
//  Messenger
//
//  Created by Muhammad on 07.12.2022.
//

struct LoginData {
    let email: String
    let password: String
}
