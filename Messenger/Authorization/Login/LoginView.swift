//
//  LoginView.swift
//  Messenger
//
//  Created by Muhammad on 05.12.2022.
//

import UIKit

final class LoginView: UIView {
    
    lazy var emailTextField = AuthorizationViewFactory.makeTextField(placeholder: "Enter your email")
    lazy var passwordTextField = AuthorizationViewFactory.makeTextField(placeholder: "Enter your password")
    lazy var loginButton = AuthorizationViewFactory.makeActionButton(string: "Login")
    
    private lazy var greetingLabel = AuthorizationViewFactory.makeLabel(string: "Welcome back! Glad to see you, Again")
    
    private let socialNetworksView = SocialNetworksView()
    
    private let haveAccountLabel: UILabel = {
        let label = UILabel()
        label.text = "Dont have an account?"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name:"Urbanist-SemiBold", size: 30)
        return label
    }()
    
    let registerAccountButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.systemGreen, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Register Now", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        return button
    }()
    
    private let haveAccountStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = Metrics.halfModule
        return stackView
    }()
    
    private let textFieldsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = Metrics.module * 2
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Layout
    private func setupLayout() {
        setupGreetingLabelLayout()
        setupTextFieldsStackViewLayout()
        setupLoginButtonLayout()
        setupSocialNetworksView()
        setupHaveAccountStackViewLayout()
    }
    
    private func setupGreetingLabelLayout() {
        addSubview(greetingLabel)
        
        greetingLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Metrics.halfModule * 5).isActive = true
        greetingLabel.topAnchor.constraint(equalTo: topAnchor, constant: Metrics.module * 15).isActive = true
        greetingLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Metrics.halfModule * 5).isActive = true
    }
    
    private func setupTextFieldsStackViewLayout() {
        addSubview(textFieldsStackView)
        
        textFieldsStackView.addArrangedSubview(emailTextField)
        textFieldsStackView.addArrangedSubview(passwordTextField)
        
        textFieldsStackView.topAnchor.constraint(equalTo: greetingLabel.bottomAnchor, constant: Metrics.module * 4).isActive = true
        textFieldsStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Metrics.halfModule * 5).isActive = true
        textFieldsStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Metrics.halfModule * 5).isActive = true
        
        emailTextField.heightAnchor.constraint(equalToConstant: Metrics.module * 7).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: Metrics.module * 7).isActive = true
    }
    
    private func setupLoginButtonLayout() {
        addSubview(loginButton)
        
        loginButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Metrics.halfModule * 5).isActive = true
        loginButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Metrics.halfModule * 5).isActive = true
        loginButton.topAnchor.constraint(equalTo: textFieldsStackView.bottomAnchor, constant: Metrics.halfModule * 15).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: Metrics.module * 7).isActive = true
    }
    
    private func setupSocialNetworksView() {
        addSubview(socialNetworksView)
        
        socialNetworksView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        socialNetworksView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        socialNetworksView.topAnchor.constraint(equalTo: loginButton.bottomAnchor).isActive = true
    }
    
    private func setupHaveAccountStackViewLayout() {
        addSubview(haveAccountStackView)
        
        haveAccountStackView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        haveAccountStackView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -Metrics.module * 4).isActive = true
        haveAccountStackView.addArrangedSubview(haveAccountLabel)
        haveAccountStackView.addArrangedSubview(registerAccountButton)
    }
}
