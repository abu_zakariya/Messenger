//
//  LoginViewController.swift
//  Messenger
//
//  Created by Muhammad on 30.11.2022.
//

import UIKit
import FirebaseAuth
import JGProgressHUD

final class LoginViewController: UIViewController {
    
    private let mainView = LoginView()
    
    private let spinner = JGProgressHUD(style: .dark)
    
    override func loadView() {
        super.loadView()
        
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchDown)
        
        mainView.registerAccountButton.addTarget(self, action: #selector(registerAccountButtonTapped), for: .touchDown)
        
        view.backgroundColor = .white
        mainView.emailTextField.delegate = self
        mainView.passwordTextField.isSecureTextEntry = true
        mainView.passwordTextField.delegate = self
    }
    
    @objc
    private func loginButtonTapped() {
        guard let email = mainView.emailTextField.text,
              let password = mainView.passwordTextField.text,
              !email.isEmpty,
              !password.isEmpty
        else {
            return showAlert(title: "Строки не могут быть пустыми", message: "")
        }
        
        if email.count < 6 || password.count < 6 {
            return showAlert(title: "Не меньше 6 символов", message: "")
        }
        
        spinner.show(in: mainView)
        
        FirebaseAuth.Auth.auth().signIn(withEmail: email, password: password) { [weak self] result, error in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                self.spinner.dismiss(animated: false)
            }
            
            guard result != nil, error == nil else {
                self.showAlert(title: "Ошибка авторизации", message: "")
                return
            }
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ок", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    @objc
    private func registerAccountButtonTapped() {
        let vc = RegisterViewController()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == mainView.emailTextField {
            mainView.passwordTextField.becomeFirstResponder()
        }
        if textField == mainView.passwordTextField {
            loginButtonTapped()
        }
        return true
    }
    
}
