//
//  Metrics.swift
//  Messenger
//
//  Created by Muhammad on 01.12.2022.
//

import UIKit

struct Metrics {
    static let halfModule: CGFloat = 4
    static let module: CGFloat = halfModule * 2 // 8
    static let doubleModule: CGFloat = module * 2 // 16
}
