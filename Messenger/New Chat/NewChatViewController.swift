//
//  NewChatViewController.swift
//  Messenger
//
//  Created by Muhammad on 04.01.2023.
//

import UIKit

final class NewChatViewController: UIViewController {
    
    private let mainView = NewChatView()
    
    private let searchBar: UISearchBar = {
        let earchBar = UISearchBar()
        earchBar.translatesAutoresizingMaskIntoConstraints = false
        earchBar.placeholder = "Поиск"
        return earchBar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        
        navigationController?.navigationBar.topItem?.titleView = searchBar
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(cancelButtonTapped))
    }
    
    override func loadView() {
        view = mainView
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchBar.becomeFirstResponder()
    }
    
    @objc
    private func cancelButtonTapped() {
        dismiss(animated: true, completion: nil)
    }
}
