//
//  ChatViewController.swift
//  Messenger
//
//  Created by Muhammad on 03.01.2023.
//

import UIKit
import MessageKit

struct Message: MessageType {
    var sender: SenderType
    var messageId: String
    var sentDate: Date
    var kind: MessageKind
}

struct Sender: SenderType {
    var senderId: String
    var displayName: String
}

final class ChatViewController: MessagesViewController {

    private let mainView = ChatView()
    
    private let sender = Sender(senderId: "1", displayName: "gazali")
    
    
    lazy var messages = [
        Message(sender: sender, messageId: "1", sentDate: Date(timeIntervalSince1970: 1000000), kind: .text("Салам алейкум")),
        Message(sender: sender, messageId: "2", sentDate: Date(), kind: .text("мух ву хьо ваша")),
        Message(sender: sender, messageId: "3", sentDate: Date(), kind: .text("Са декхар хьа мац ло ахь со кхуз леш ма ву")),
        Message(sender: sender, messageId: "4", sentDate: Date(), kind: .text("ас нийс 5 де хан ло хьун ахь хьа ца лохь ас цецвокхар ву хьо"))
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
    }
    
    override func loadView() {
        view = mainView
    }
}

extension ChatViewController: MessagesDataSource, MessagesLayoutDelegate, MessagesDisplayDelegate {
    
    func currentSender() -> SenderType {
        sender
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        messages[indexPath.section]
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        messages.count
    }
}
